from django.urls import path, re_path, include
from django.contrib import admin
from django.conf import settings

from rest_framework import urls as drf_urls

from {{ cookiecutter.repo_name }} import api_urls
from {{ cookiecutter.repo_name }}.views import (EmberView, MeView, LoginView, LogoutView,
                                                service_worker_view, root_files_view)

urlpatterns = []

if settings.DEBUG:
    urlpatterns = [
        re_path(r'(?P<filetype>(service-worker|sw-toolbox))(?P<buildhash>(-\w*)?)\.js$', service_worker_view),
        re_path(r'^(?P<filename>(crossdomain\.xml|manifest\.appcache|robots\.txt))$', root_files_view),
    ]

urlpatterns += [

    path('admin/', admin.site.urls),
    path('api/v1/', include(api_urls)),
    path('api-auth/', include(drf_urls)),
    path('api/auth/login/', LoginView.as_view()),
    path('api/auth/logout/', LogoutView.as_view()),
    path('api/auth/me/', MeView.as_view()),

    re_path(r'', EmberView.as_view(template_name='index.html'), name='ember'),
]
